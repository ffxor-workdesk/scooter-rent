﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using ScooterRent.Models;
using ScooterRent.Models.Interface;
using ScooterRent.Services;
using ScooterRent.Services.Interface;

namespace ScooterRent
{
	public class Startup
	{
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<IRentalCompany, RentalCompany>();
			services.AddTransient<IScooterService, ScooterService>();
			services.AddTransient<IDateTimeProvider, DateTimeProvider>();
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.Run(async (context) =>
			{
				await context.Response.WriteAsync("Faster! Harder! Scooter!");
			});
		}
	}
}
