﻿using System;
using System.Collections.Generic;
using ScooterRent.Models;
using ScooterRent.Models.Interface;

namespace ScooterRent.Services.Interface
{
	public interface IScooterService
	{
		/// <summary>
		/// Add <see cref="Scooter"/>
		/// </summary>
		/// <param name="id">Unique ID of the <see cref="Scooter"/></param>
		/// <param name="pricePerMinute">Rental price of the <see cref="Scooter"/> per one minute</param>
		/// <exception cref="ArgumentException">When <see cref="Scooter.Id"/> is non unique</exception>
		void AddScooter(string id, decimal pricePerMinute);

		/// <summary>
		/// Remove <see cref="Scooter"/>. This action is not allowed for <see cref="Scooter"/> if the rental is in progress
		/// </summary>
		/// <param name="id"></param>
		/// <exception cref="KeyNotFoundException">When a <see cref="Scooter"/> does not exist</exception>
		/// <exception cref="InvalidOperationException">When a <see cref="Scooter"/> is not able to be removed</exception>
		void RemoveScooter(string id);

		/// <summary>
		/// List of <see cref="Scooter"/> that belong to the <see cref="IRentalCompany"/>
		/// </summary>
		/// <returns>Return a list of available <see cref="Scooter"/></returns>
		IList<Scooter> GetScooters();

		/// <summary>
		/// Get particular <see cref="Scooter"/> by ID
		/// </summary>
		/// <param name="scooterId">Unique ID of the <see cref="Scooter"/></param>
		/// <returns>Return a particular <see cref="Scooter"/></returns>
		/// <exception cref="KeyNotFoundException">When the <see cref="Scooter"/> was not found</exception>
		Scooter GetScooterById(string scooterId);
	}
}
