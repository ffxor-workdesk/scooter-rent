﻿using System;

namespace ScooterRent.Services.Interface
{
	public interface IDateTimeProvider
	{
		DateTime Now { get; }
	}
}
