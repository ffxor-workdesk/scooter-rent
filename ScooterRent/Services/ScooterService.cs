﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScooterRent.Services.Interface;
using ScooterRent.Models;

namespace ScooterRent.Services
{
	public class ScooterService : IScooterService
	{
		protected IList<Scooter> scooters = new List<Scooter>();

		public void AddScooter(string id, decimal pricePerMinute)
		{
			Scooter scooter;
			try
			{
				scooter = this.GetScooterById(id);
				throw new ArgumentException($"The {nameof(Scooter)} with specified id already exists");
			}
			catch (KeyNotFoundException)
			{
				scooter = new Scooter(id, pricePerMinute);
			}
			  
			this.scooters.Add(scooter);
 		}

		public void RemoveScooter(string id)
		{
			var scooter = this.GetScooterById(id);
			if (scooter.IsRented)
				throw new InvalidOperationException($"The {nameof(Scooter)} with specified id is in rent now");

			this.scooters.Remove(scooter);
		}

		public Scooter GetScooterById(string scooterId) => 
			this.scooters.Where(t => string.Equals(t.Id, scooterId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()
			?? throw new KeyNotFoundException($"The {nameof(Scooter)} with specified id was not found");

		public IList<Scooter> GetScooters() => this.scooters;
	}
}
