﻿using System;
using ScooterRent.Services.Interface;

namespace ScooterRent.Services
{
	public class DateTimeProvider : IDateTimeProvider
	{
		public DateTime Now { get; } = DateTime.Now;
	}
}
