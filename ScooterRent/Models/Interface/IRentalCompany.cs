﻿using System;
using ScooterRent.Services.Interface;

namespace ScooterRent.Models.Interface
{
	public interface IRentalCompany
	{
		/// <summary>
		/// Provides access to <see cref="Scooter"/> managing service
		/// </summary>
		IScooterService ScooterService { get; }

		/// <summary>
		/// Name of the company
		/// </summary>
		string Name { get; }

		/// <summary>
		/// Start the rent of the <see cref="Scooter"/>
		/// </summary>
		/// <param name="id">ID of the <see cref="Scooter"/></param>
		/// <exception cref="InvalidOperationException">When a <see cref="Scooter"/> is not able to be rented</exception>
		void StartRent(string id);

		/// <summary>
		/// End the rent of the <see cref="IScooter"/>
		/// </summary>
		/// <param name="id">ID of the <see cref="IScooter"/></param>
		/// <returns>The total price of rental</returns>
		/// <remarks>It has to calculated taking into account for how long time <see cref="Scooter"/> was rented.
		/// If total amount per day reaches 20 EUR than timer must be stopped and restarted at beginning of next day at 0:00 am</remarks>
		/// <exception cref="InvalidOperationException">When rent of <see cref="Scooter"/> is not able to be finished</exception>
		decimal EndRent(string id);

		/// <summary>
		/// Income report
		/// </summary>
		/// <param name="year">Year of the report. Sum all years if value is not set</param>
		/// <param name="includeNotCompletedRentals">Include income from the <see cref="Scooter"/> that are rented out (rental has not ended yet) 
		/// and calculate rental price as if the rental would end at the time when this report was requested</param>
		/// <returns>The total price of all rentals filtered by year if given</returns>
		decimal CalculateIncome(int? year, bool includeNotCompletedRentals);
	}
}
