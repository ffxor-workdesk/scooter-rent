﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ScooterRent.Models.Interface;

namespace ScooterRent.Models
{
	public class Scooter
	{
		public Scooter(string id, decimal pricePerMinute)
		{
			if (pricePerMinute < 0)
				throw new ArgumentException("Price could not be less than 0", nameof(pricePerMinute));

			this.Id = id;
			this.PricePerMinute = pricePerMinute;
		}

		/// <summary>
		/// Unique ID of the scooter
		/// </summary>
		public string Id { get; }

		/// <summary>
		/// Rental price of the scooter per one minute
		/// </summary>
		public decimal PricePerMinute { get; }

		/// <summary>
		/// Identify if someone is renting this scooter
		/// </summary>
		public bool IsRented { get; set; }
	}
}
