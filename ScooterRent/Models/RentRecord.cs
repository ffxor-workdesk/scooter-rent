﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScooterRent.Models
{
	public class RentRecord
	{
		public string ScooterId { get; }

		public DateTime StartRentDateTime { get; }

		public DateTime? EndRentDateTime { get; set; }

		public decimal Income { get; set; }

		public RentRecord(string scooterId, DateTime startRentDateTime)
		{
			this.ScooterId = scooterId;
			this.StartRentDateTime = startRentDateTime;
		}
	}
}
