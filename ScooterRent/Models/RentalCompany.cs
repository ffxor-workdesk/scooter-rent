﻿using System;
using System.Collections.Generic;
using System.Linq;

using ScooterRent.Models.Interface;
using ScooterRent.Services.Interface;

namespace ScooterRent.Models
{
	public class RentalCompany : IRentalCompany
	{
		public string Name { get; set; } = "DefaultCompany";
		public IScooterService ScooterService { get; }
		protected IDateTimeProvider dateTimeProvider;
		protected const decimal DAY_FEE_LIMIT = 20;
		protected const int DAY_MINUTES = 1440;

		protected IList<RentRecord> rentRecords = new List<RentRecord>();

		public RentalCompany(IScooterService scooterService, IDateTimeProvider dateTimeProvider)
		{
			this.ScooterService = scooterService;
			this.dateTimeProvider = dateTimeProvider;
		}

		public void StartRent(string id)
		{
			var scooter = this.ScooterService.GetScooterById(id);
			if (scooter.IsRented)
				throw new InvalidOperationException($"The {nameof(Scooter)} with specified id is in rent now");

			scooter.IsRented = true;

			this.rentRecords.Add(new RentRecord(id, dateTimeProvider.Now));
		}

		public decimal EndRent(string id)
		{
			var scooter = this.ScooterService.GetScooterById(id);
			if (!scooter.IsRented)
				throw new InvalidOperationException($"The {nameof(Scooter)} with specified id is not in rent now");

			scooter.IsRented = false;

			var record = this.rentRecords.FirstOrDefault(t => string.Equals(t.ScooterId, id, StringComparison.InvariantCultureIgnoreCase) && !t.EndRentDateTime.HasValue)
				?? throw new KeyNotFoundException($"The {nameof(RentRecord)} for the {nameof(Scooter)} with specified id was not found");
			record.EndRentDateTime = dateTimeProvider.Now;

			record.Income = this.calculateFee(record.StartRentDateTime, record.EndRentDateTime.Value, scooter.PricePerMinute);

			return record.Income;
		}

		protected decimal calculateFee(DateTime startRentDateTime, DateTime endRentDateTime, decimal pricePerMinute)
		{
			decimal rentalFee = 0;
			int minutesSection;

			Func<int, decimal> calculate = (minutes) => {
				var fee = ((minutes > DAY_MINUTES) ? DAY_MINUTES : minutes) * pricePerMinute;
				return (fee <= DAY_FEE_LIMIT) ? fee : DAY_FEE_LIMIT;
			};

			var milestone = startRentDateTime;

			// Calculate all rent days before the last one
			while (milestone.Date != endRentDateTime.Date)
			{
				// End of day
				var eod = milestone.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

				minutesSection = Convert.ToInt32(Math.Round((eod - milestone).TotalMinutes, 0));
				rentalFee += calculate(minutesSection);

				// Move to the next day
				milestone = eod.AddSeconds(1);
			}

			// Calculate rest of the last rent day
			minutesSection = Convert.ToInt32(Math.Round((endRentDateTime - milestone).TotalMinutes, 0));
			rentalFee += calculate(minutesSection);

			return rentalFee;
		}

		public decimal CalculateIncome(int? year, bool includeNotCompletedRentals)
		{
			var records = this.rentRecords.Where(t =>
				(!year.HasValue || year.HasValue && t.StartRentDateTime.Year == year) &&
				(includeNotCompletedRentals || !includeNotCompletedRentals && t.EndRentDateTime.HasValue));

			var income = records.Sum(t =>
			{
				if (t.EndRentDateTime.HasValue)
				{
					return t.Income;
				}
				else
				{
					var scooter = this.ScooterService.GetScooterById(t.ScooterId);
					return this.calculateFee(t.StartRentDateTime, this.dateTimeProvider.Now, scooter.PricePerMinute);
				}	
			});

			return income;
		}
	}
}
