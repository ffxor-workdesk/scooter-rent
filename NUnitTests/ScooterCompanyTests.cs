﻿using System;
using NUnit.Framework;
using Moq;
using System.Collections.Generic;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using ScooterRent.Models;
using ScooterRent.Models.Interface;
using ScooterRent.Services.Interface;

namespace ScooterRent.Tests
{
	public class ScooterCompanyTests
	{
		IRentalCompany rentalCompany;
		IScooterService scooterService;

		[SetUp]
		public void Setup()
		{
			// Create WebHost of the testing web application
			var webHost = WebHost.CreateDefaultBuilder()
				.UseStartup<Startup>()
				.Build();

			// Retrieve instances using configured DI in the web application
			this.rentalCompany = (IRentalCompany)webHost.Services.GetService(typeof(IRentalCompany));
			this.scooterService = (IScooterService)webHost.Services.GetService(typeof(IScooterService));
		}

		/// <summary>
		/// Check than start rent of <see cref="Scooter"/> works properly
		/// </summary>
		[Test]
		public void StartRent_Succeed()
		{
			const string scooterId = "S01";
			const decimal pricePerMinute = 0.1M;

			this.rentalCompany.ScooterService.AddScooter(scooterId, pricePerMinute);
			this.rentalCompany.StartRent(scooterId);
			var scooter = this.rentalCompany.ScooterService.GetScooterById(scooterId);

			Assert.True(scooter.IsRented, "Wrong scooter state");
		}

		/// <summary>
		/// Check whether an exception is thrown when trying to start rent of <see cref="Scooter"/> which haven't been added before or the <see cref="Scooter"/> is being in rent
		/// </summary>
		[Test]
		public void StartRent_NonExistingOrNonRentedScooter_Failed()
		{
			const string scooterId = "S01";
			const decimal pricePerMinute = 0.1M;

			this.rentalCompany.ScooterService.AddScooter(scooterId, pricePerMinute);
			var scooter = this.rentalCompany.ScooterService.GetScooterById(scooterId);
			scooter.IsRented = true;

			Assert.Multiple(() =>
			{
				Assert.Throws(typeof(KeyNotFoundException), () => this.rentalCompany.StartRent(string.Empty), "Wrong exception type");
				Assert.Throws(typeof(InvalidOperationException), () => this.rentalCompany.StartRent(scooterId), "Wrong exception type");
			});
		}

		/// <summary>
		/// Check than end rent of <see cref="Scooter"/> works properly
		/// </summary>
		[Test]
		[TestCase("06.02.2020 12:00", "06.02.2020 12:54", .1, 5.4)]
		[TestCase("06.02.2020 12:00", "06.02.2020 17:00", .5, 20)]
		[TestCase("06.02.2020 12:00", "07.02.2020 18:00", .5, 40)]
		[TestCase("06.02.2020 22:00", "07.02.2020 05:00", .1, 32)]
		public void EndRent_Succeed(string startTime, string endTime, decimal testPricePerMinute, decimal testRentalFee)
		{
			const string scooterId = "S01";

			// Create mock of datetime provider for company rent calculation
			var dateTimeProviderStub = new Mock<IDateTimeProvider>();
			dateTimeProviderStub.SetupSequence(p => p.Now)
				.Returns(DateTime.Parse(startTime))       // When rent was started
				.Returns(DateTime.Parse(endTime));        // When rent was finished

			var company = new RentalCompany(this.scooterService, dateTimeProviderStub.Object);

			company.ScooterService.AddScooter(scooterId, testPricePerMinute);
			var scooter = company.ScooterService.GetScooterById(scooterId);

			company.StartRent(scooterId);
			var total = company.EndRent(scooterId);

			Assert.Multiple(() =>
			{
				Assert.IsFalse(scooter.IsRented, "Wrong scooter state");
				Assert.AreEqual(testRentalFee, total, "Wrong result of calculating rental fee");
			});
		}

		/// <summary>
		/// Check whether an exception is thrown when trying to end rent of <see cref="Scooter"/> which haven't been added before or the <see cref="Scooter"/> is not being in rent
		/// </summary>
		[Test]
		public void EndRent_NonExistingOrNonRentedScooter_Failed()
		{
			const string scooterId = "S01";
			const decimal pricePerMinute = 0.1M;

			this.rentalCompany.ScooterService.AddScooter(scooterId, pricePerMinute);
			var scooter = this.rentalCompany.ScooterService.GetScooterById(scooterId);
			scooter.IsRented = false; // it's false by default but to be obvious

			Assert.Multiple(() =>
			{
				Assert.Throws(typeof(KeyNotFoundException), () => this.rentalCompany.EndRent(string.Empty), "Wrong exception type");
				Assert.Throws(typeof(InvalidOperationException), () => this.rentalCompany.EndRent(scooterId), "Wrong exception type");
			});
		}

		/// <summary>
		/// Check whether company's income is calculated properly
		/// </summary>
		[Test]
		[TestCase(1, .1, 6, 12)]
		public void CalculateIncome_Succeed(double testPeriod, decimal testPricePerMinute, decimal testIncomeFiltered, decimal testIncomeOverall)
		{
			const string scooterId_1 = "S01";
			const string scooterId_2 = "S02";

			// Create mock of datetime provider for company rent calculation
			var dateTimeProviderStub = new Mock<IDateTimeProvider>();
			dateTimeProviderStub.SetupSequence(p => p.Now)
				// First calculation
				.Returns(DateTime.Now)                              // When the 1st rent was started
				.Returns(DateTime.Now.AddHours(testPeriod))         // When the 1st rent was finished
				.Returns(DateTime.Now.AddHours(testPeriod + 1))		// When the 2nd rent was started
				.Returns(DateTime.Now.AddHours(testPeriod + 2))     // When calculation proceed
				// Second calculation
				.Returns(DateTime.Now)                              // When the 1st rent was started
				.Returns(DateTime.Now.AddHours(testPeriod))			// When the 1st rent was finished
				.Returns(DateTime.Now.AddHours(testPeriod + 1))     // When the 2nd rent was started
				.Returns(DateTime.Now.AddHours(testPeriod + 2));	// When calculation proceed

			var company = new RentalCompany(this.scooterService, dateTimeProviderStub.Object);

			company.ScooterService.AddScooter(scooterId_1, testPricePerMinute);
			company.ScooterService.AddScooter(scooterId_2, testPricePerMinute);

			company.StartRent(scooterId_1);
			company.EndRent(scooterId_1);

			company.StartRent(scooterId_2);

			var incomeFiltered = company.CalculateIncome(DateTime.Now.Year, includeNotCompletedRentals: false);
			var incomeOverall = company.CalculateIncome(null, includeNotCompletedRentals: true);

			Assert.Multiple(() =>
			{
				Assert.AreEqual(incomeFiltered, testIncomeFiltered, "Wrong result of calculating filtered income");
				Assert.AreEqual(incomeOverall, testIncomeOverall, "Wrong result of calculating overall income");
			});
		}
	}
}
