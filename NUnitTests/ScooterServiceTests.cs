using System;
using NUnit.Framework;
using System.Collections.Generic;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using ScooterRent.Models;
using ScooterRent.Models.Interface;
using ScooterRent.Services.Interface;

namespace ScooterRent.Tests
{
	public class ScooterServiceTests
	{
		readonly string scooterId = "S01";
		readonly decimal scooterPricePerMinute = 0.5M;

		IScooterService scooterService;
		Action addScooter;

		[SetUp]
		public void Setup()
		{
			// Create WebHost of the testing web application
			var webHost = WebHost.CreateDefaultBuilder()
				.UseStartup<Startup>()
				.Build();

			// Retrieve instances using configured DI in the web application
			this.scooterService = (IScooterService)webHost.Services.GetService(typeof(IScooterService));

			// Delegate to shortcut the action
			this.addScooter = () => this.scooterService.AddScooter(this.scooterId, this.scooterPricePerMinute);
		}

		/// <summary>
		/// Check than adding <see cref="Scooter"/> works properly
		/// </summary>
		[Test]
		public void AddScooter_Succeed()
		{
			addScooter();
			var scooters = this.scooterService.GetScooters();

			Assert.Multiple(() =>
			{
				Assert.AreEqual(scooters.Count, 1, $"Unexpected amount of {nameof(Scooter)}");
				Assert.AreEqual(scooters[0].Id, this.scooterId, $"Mismatch data in {nameof(Scooter.Id)}");
				Assert.AreEqual(scooters[0].PricePerMinute, this.scooterPricePerMinute, $"Mismatch data in {nameof(Scooter.PricePerMinute)}");
				Assert.AreEqual(scooters[0].IsRented, false, $"Mismatch data in {nameof(Scooter.IsRented)}");
			}); 
		}

		/// <summary>
		/// Check whether an exception is thrown when trying to add more than one <see cref="Scooter"/> with the same id
		/// </summary>
		[Test]
		public void AddScooter_WithNonUniqId_Failed()
		{
			addScooter();

			Assert.Throws(typeof(ArgumentException), () => addScooter(), $"Wrong exception type");
		}

		/// <summary>
		/// Check than removing <see cref="Scooter"/> works properly
		/// </summary>
		[Test]
		public void RemoveScooter_Succeed()
		{
			addScooter();
			this.scooterService.RemoveScooter(this.scooterId);
			var scooters = this.scooterService.GetScooters();

			Assert.IsEmpty(scooters, $"Unexpected amount of {nameof(Scooter)}");
		}

		/// <summary>
		/// Check whether an exception is thrown when trying to remove the <see cref="Scooter"/> which haven't been added before or the <see cref="Scooter"/> is being in rent
		/// </summary>
		[Test]
		public void RemoveScooter_NonExistingOrRentingScooter_Failed()
		{
			addScooter();
			var scooter = this.scooterService.GetScooterById(this.scooterId);
			scooter.IsRented = true;

			Assert.Multiple(()=>
			{
				Assert.Throws(typeof(KeyNotFoundException), () => this.scooterService.RemoveScooter(string.Empty), "Wrong exception type");
				Assert.Throws(typeof(InvalidOperationException), () => this.scooterService.RemoveScooter(this.scooterId), "Wrong exception type");
			});
		}
	}
}